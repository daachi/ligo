open Datatypes

module Nat :
 sig
  val eqb : nat -> nat -> bool
 end
